﻿namespace Programming_Project_5
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BirthdayLabel = new System.Windows.Forms.Label();
            this.FavoriteColorLabel = new System.Windows.Forms.Label();
            this.BirthYearLabel = new System.Windows.Forms.Label();
            this.BirthMonthLabel = new System.Windows.Forms.Label();
            this.BirthDayComboBox = new System.Windows.Forms.ComboBox();
            this.FavoriteColorComboBox = new System.Windows.Forms.ComboBox();
            this.BirthYearComboBox = new System.Windows.Forms.ComboBox();
            this.BirthMonthComboBox = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BirthdayLabel
            // 
            this.BirthdayLabel.AutoSize = true;
            this.BirthdayLabel.Location = new System.Drawing.Point(12, 9);
            this.BirthdayLabel.Name = "BirthdayLabel";
            this.BirthdayLabel.Size = new System.Drawing.Size(136, 17);
            this.BirthdayLabel.TabIndex = 0;
            this.BirthdayLabel.Text = "Enter your Birth Day";
            // 
            // FavoriteColorLabel
            // 
            this.FavoriteColorLabel.AutoSize = true;
            this.FavoriteColorLabel.Location = new System.Drawing.Point(12, 119);
            this.FavoriteColorLabel.Name = "FavoriteColorLabel";
            this.FavoriteColorLabel.Size = new System.Drawing.Size(181, 17);
            this.FavoriteColorLabel.TabIndex = 1;
            this.FavoriteColorLabel.Text = "What is your favorite color?";
            // 
            // BirthYearLabel
            // 
            this.BirthYearLabel.AutoSize = true;
            this.BirthYearLabel.Location = new System.Drawing.Point(12, 79);
            this.BirthYearLabel.Name = "BirthYearLabel";
            this.BirthYearLabel.Size = new System.Drawing.Size(141, 17);
            this.BirthYearLabel.TabIndex = 2;
            this.BirthYearLabel.Text = "Enter your Birth Year";
            // 
            // BirthMonthLabel
            // 
            this.BirthMonthLabel.AutoSize = true;
            this.BirthMonthLabel.Location = new System.Drawing.Point(12, 45);
            this.BirthMonthLabel.Name = "BirthMonthLabel";
            this.BirthMonthLabel.Size = new System.Drawing.Size(150, 17);
            this.BirthMonthLabel.TabIndex = 3;
            this.BirthMonthLabel.Text = "Enter your Birth Month";
            // 
            // BirthDayComboBox
            // 
            this.BirthDayComboBox.FormattingEnabled = true;
            this.BirthDayComboBox.Location = new System.Drawing.Point(224, 6);
            this.BirthDayComboBox.Name = "BirthDayComboBox";
            this.BirthDayComboBox.Size = new System.Drawing.Size(121, 24);
            this.BirthDayComboBox.TabIndex = 4;
            // 
            // FavoriteColorComboBox
            // 
            this.FavoriteColorComboBox.FormattingEnabled = true;
            this.FavoriteColorComboBox.Location = new System.Drawing.Point(224, 116);
            this.FavoriteColorComboBox.Name = "FavoriteColorComboBox";
            this.FavoriteColorComboBox.Size = new System.Drawing.Size(121, 24);
            this.FavoriteColorComboBox.TabIndex = 5;
            // 
            // BirthYearComboBox
            // 
            this.BirthYearComboBox.FormattingEnabled = true;
            this.BirthYearComboBox.Location = new System.Drawing.Point(224, 76);
            this.BirthYearComboBox.Name = "BirthYearComboBox";
            this.BirthYearComboBox.Size = new System.Drawing.Size(121, 24);
            this.BirthYearComboBox.TabIndex = 6;
            // 
            // BirthMonthComboBox
            // 
            this.BirthMonthComboBox.FormattingEnabled = true;
            this.BirthMonthComboBox.Location = new System.Drawing.Point(224, 42);
            this.BirthMonthComboBox.Name = "BirthMonthComboBox";
            this.BirthMonthComboBox.Size = new System.Drawing.Size(121, 24);
            this.BirthMonthComboBox.TabIndex = 7;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(35, 157);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(298, 63);
            this.button1.TabIndex = 8;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(552, 382);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.BirthMonthComboBox);
            this.Controls.Add(this.BirthYearComboBox);
            this.Controls.Add(this.FavoriteColorComboBox);
            this.Controls.Add(this.BirthDayComboBox);
            this.Controls.Add(this.BirthMonthLabel);
            this.Controls.Add(this.BirthYearLabel);
            this.Controls.Add(this.FavoriteColorLabel);
            this.Controls.Add(this.BirthdayLabel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label BirthdayLabel;
        private System.Windows.Forms.Label FavoriteColorLabel;
        private System.Windows.Forms.Label BirthYearLabel;
        private System.Windows.Forms.Label BirthMonthLabel;
        private System.Windows.Forms.ComboBox BirthDayComboBox;
        private System.Windows.Forms.ComboBox FavoriteColorComboBox;
        private System.Windows.Forms.ComboBox BirthYearComboBox;
        private System.Windows.Forms.ComboBox BirthMonthComboBox;
        private System.Windows.Forms.Button button1;
    }
}

