﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programming_Project_5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            int days = 31;
            int[] today = new int[days];

            for (int i = 1; i < days; i++)
            {
                today[i] = i;
            }

            for (int i = 1; i < today.Length; i++)
            {
                BirthDayComboBox.Items.Add(today[i]);
            }

            int months = 12;
            string[] month = new string[months];
            month[0] = "January";
            month[1] = "February";
            month[2] = "March";
            month[3] = "April";
            month[4] = "May";
            month[5] = "June";
            month[6] = "July";
            month[7] = "August";
            month[8] = "September";
            month[9] = "October";
            month[10] = "November";
            month[11] = "December";

            foreach(string item in month)
            {
                BirthMonthComboBox.Items.Add(item.ToString());
            }

            for (int i = 1; i < days; i++)
            {
                today[i] = i + 1990;
            }

            for (int i = 1; i < today.Length; i++)
            {
                BirthYearComboBox.Items.Add(today[i]);
            }

            int color = 5;
            string[] colors = new string[color];
            month[0] = "Red";
            month[1] = "Blue";
            month[2] = "Green";
            month[3] = "Purple";
            month[4] = "Orange";

            foreach (string item in colors)
            {
                FavoriteColorComboBox.Items.Add(item.ToString());
            }
        }
    }
}
